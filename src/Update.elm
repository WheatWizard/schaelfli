module Update
  exposing
    ( update
    )


import Geometry.Grid
  as Grid
import Geometry.Position
  as Position
import Type.Message
  as Message
  exposing
    ( Message
    )
import Type.Model
  exposing
    ( Model (..)
    )


update : Message -> Model -> (Model, Cmd Message)
update message (Model { source, program, position }) =
  case
    message
  of
    Message.Change newSource ->
      ( Model
        { source =
          newSource
        , program =
          Grid.fromSource newSource
        , position =
          position
        }
      , Cmd.none
      )
    Message.Compile ->
      ( Model
        { source =
          source
        , program =
          Grid.fromSource source
        , position =
          Position.origin
        }
      , Cmd.none
      )
    Message.Step ->
      ( Model
        { source =
          source
        , program =
          program
        , position =
          Position.step position
        }
      , Cmd.none
      )
    Message.TurnLeft ->
      ( Model
        { source =
          source
        , program =
          program
        , position =
          Position.rotateLeft position
        }
      , Cmd.none
      )
