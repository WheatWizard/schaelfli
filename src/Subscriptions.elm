module Subscriptions
  exposing
    ( subscriptions
    )


import Type.Message
  exposing
    ( Message
    )
import Type.Model
  exposing
    ( Model
    )


subscriptions : Model -> Sub Message
subscriptions _ =
  Sub.none
