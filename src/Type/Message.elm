module Type.Message
  exposing
    ( Message (..)
    )


type Message
  = Change String
  | Compile
  | Step
  | TurnLeft
