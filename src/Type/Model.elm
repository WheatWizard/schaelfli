module Type.Model
  exposing
    ( Model (..)
    )


import Geometry.Grid
  exposing
    ( Grid
    )
import Geometry.Position
  exposing
    ( Position
    )


type Model =
  Model
    { source :
      String
    , program :
      Grid
    , position :
      Position
    }
