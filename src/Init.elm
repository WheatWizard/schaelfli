module Init
  exposing
    ( init
    )


import Geometry.Grid
  as Grid
import Geometry.Position
  as Position
import Type.Message
  exposing
    ( Message
    )
import Type.Model
  exposing
    ( Model (..)
    )


init : a -> (Model, Cmd Message)
init _ =
  ( Model
    { source =
      ""
    , program =
      Grid.empty
    , position =
      Position.origin
    }
  , Cmd.none
  )
