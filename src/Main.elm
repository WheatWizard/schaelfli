module Main
  exposing
    ( main
    )


import Browser


import Type.Model
  exposing
    ( Model
    )
import Type.Message
  exposing
    ( Message
    )
import Init
import Update
import View
import Subscriptions


main : Program () Model Message
main =
  Browser.document
    { init =
      Init.init
    , update =
      Update.update
    , view =
      View.view
    , subscriptions =
      Subscriptions.subscriptions
    }
