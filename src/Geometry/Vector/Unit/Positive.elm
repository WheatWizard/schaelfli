module Geometry.Vector.Unit.Positive
  exposing
    ( PositiveUnitVector (..)
    )


type PositiveUnitVector
  = I
  | J
  | K
