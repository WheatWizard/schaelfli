module Geometry.Vector.Unit
  exposing
    ( UnitVector
    , i
    , j
    , k
    , cross
    , negate
    , add
    , toPositive
    )


import Maybe
  exposing
    ( Maybe (..)
    )


import Geometry.Vector.Unit.Positive
  as Positive
  exposing
    ( PositiveUnitVector
    )
import Geometry.Vector
   exposing
     ( Vector
     )


type UnitVector
  = UV Vector


i : UnitVector
i =
  UV (1, 0, 0)


j : UnitVector
j =
  UV (0, 1, 0)


k : UnitVector
k =
  UV (0, 0, 1)


cross : UnitVector -> UnitVector -> UnitVector
cross (UV (x1, x2, x3)) (UV (y1, y2, y3)) =
  UV
    ( x2*y3 - y2*x3
    , x3*y1 - y3*x1
    , x1*y2 - y1*x2
    )


negate : UnitVector -> UnitVector
negate (UV (x, y, z)) =
  UV (-x, -y, -z)


add : Vector -> UnitVector -> Vector
add (x1, x2, x3) (UV (y1, y2, y3)) =
  ( x1 + y1
  , x2 + y2
  , x3 + y3
  )


toPositive : UnitVector -> Maybe PositiveUnitVector
toPositive vec =
  case
    vec
  of
    UV (1, 0, 0) ->
      Just Positive.I
    UV (0, 1, 0) ->
      Just Positive.J
    UV (0, 0, 1) ->
      Just Positive.K
    _ ->
      Nothing
