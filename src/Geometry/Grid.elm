module Geometry.Grid
  exposing
    ( Grid
    , empty
    , getCell
    , fromSource
    )


import Dict
  exposing
    ( Dict
    )


import Geometry.Position
  as Position
  exposing
    ( Position
    )
import Geometry.Vector
  exposing
    ( Vector
    )
import Geometry.Vector.Unit.Positive
  as Positive
import Queue
  exposing
    ( Queue
    )


type alias Grid =
  { cells :
    Dict Vector (Dict Int Cell)
  }


type alias Cell
  = Char


empty : Grid
empty =
  Grid Dict.empty


getCell : Grid -> Position -> Maybe Cell
getCell { cells } position =
  case
    Position.normalize position
  of
    Just (center, faceVector) ->
      case
        faceVector
      of
        Positive.I ->
          Dict.get center cells |> Maybe.andThen (Dict.get 0)
        Positive.J ->
          Dict.get center cells |> Maybe.andThen (Dict.get 1)
        Positive.K ->
          Dict.get center cells |> Maybe.andThen (Dict.get 2)
    Nothing ->
      Nothing


cellFilled : Grid -> Position -> Bool
cellFilled grid position =
  case
    getCell grid position
  of
    Nothing ->
      False
    Just _ ->
      True


putCell : Position -> Cell -> Grid -> Grid
putCell position cell { cells } =
  let
    go k v m =
      case
        m
      of
        Nothing ->
          Dict.singleton k v |> Just
        Just d ->
          Dict.insert k v d |> Just
  in
    case
      Position.normalize position
    of
      Just (center, faceVector) ->
        case
          faceVector
        of
          Positive.I ->
            Dict.update center (go 0 cell) cells |> Grid
          Positive.J ->
            Dict.update center (go 1 cell) cells |> Grid
          Positive.K ->
            Dict.update center (go 2 cell) cells |> Grid
      Nothing ->
        { cells =
          cells
        }


makeGrid : List Cell -> Grid
makeGrid =
  let
    go : Queue Position -> Grid -> List Cell -> Grid
    go queue grid source =
      case
        source
      of
        [] ->
          grid
        (x :: xs) ->
          case
            Queue.pop queue
          of
            Nothing -> -- Impossible case
              grid
            Just (pos1, poses) ->
              go
                ( Queue.pushes
                  ( Position.adjacents pos1
                    |> List.filter (not << cellFilled grid)
                  ) poses
                )
                (putCell pos1 x grid)
                xs
  in
    go (Queue.singleton Position.origin) { cells = Dict.empty }


fromSource : String -> Grid
fromSource x =
  makeGrid (String.toList x)
