module Geometry.Vector
  exposing
    ( Vector
    , odd
    , even
    )


type alias Vector =
  ( Int
  , Int
  , Int
  )


odd : Vector -> Bool
odd (x, y, z) =
  modBy 2 (x + y + z) == 1


even : Vector -> Bool
even (x, y, z) =
  modBy 2 (x + y + z) == 0
