module Geometry.Position
  exposing
    ( Position
    , origin
    , step
    , rotateLeft
    , rotateRight
    , normalize
    , adjacents
    )


import Geometry.Vector.Unit
  as UnitVector
  exposing
    ( UnitVector
    )
import Geometry.Vector.Unit.Positive
  exposing
    ( PositiveUnitVector
    )
import Geometry.Vector
  as Vector
  exposing
    ( Vector
    )


type alias Position =
  { centerOfRotation :
    Vector
  , faceVector :
    UnitVector
  , angularMoment :
    UnitVector
  }


origin : Position
origin =
  { centerOfRotation =
    (0,0,0)
  , faceVector =
    UnitVector.i
  , angularMoment =
    UnitVector.j
  }


step : Position -> Position
step {centerOfRotation, faceVector, angularMoment} =
  { centerOfRotation =
    centerOfRotation
  , angularMoment =
    angularMoment
  , faceVector =
    UnitVector.cross angularMoment faceVector
  }


rotateRelativeLeft : Position -> Position
rotateRelativeLeft {centerOfRotation, faceVector, angularMoment} =
  { centerOfRotation =
    UnitVector.add centerOfRotation faceVector
  , faceVector =
    UnitVector.negate faceVector
  , angularMoment =
    UnitVector.cross faceVector angularMoment
  }


rotateRelativeRight : Position -> Position
rotateRelativeRight {centerOfRotation, faceVector, angularMoment} =
  { centerOfRotation =
    UnitVector.add centerOfRotation faceVector
  , faceVector =
    UnitVector.negate faceVector
  , angularMoment =
    UnitVector.cross angularMoment faceVector
  }


rotateLeft : Position -> Position
rotateLeft pos =
  if
    Vector.odd pos.centerOfRotation
  then
    rotateRelativeLeft pos
  else
    rotateRelativeRight pos


rotateRight : Position -> Position
rotateRight pos =
  if
    Vector.odd pos.centerOfRotation
  then
    rotateRelativeRight pos
  else
    rotateRelativeLeft pos

normalize : Position -> Maybe ((Int, Int, Int), PositiveUnitVector)
normalize {centerOfRotation, faceVector} =
  case
    UnitVector.toPositive faceVector
  of
    Just v ->
      Just
        ( centerOfRotation
        , v
        )
    Nothing ->
      case
        UnitVector.toPositive (UnitVector.negate faceVector)
      of
        Just v ->
          Just
            ( UnitVector.add centerOfRotation faceVector
            , v
            )
        Nothing -> -- Should never happen
          Nothing


adjacents : Position -> List Position
adjacents pos =
  [ pos |> step
  , pos |> rotateLeft |> step
  , pos |> rotateLeft |> rotateLeft |> step
  , pos |> rotateRight |> step
  ]
