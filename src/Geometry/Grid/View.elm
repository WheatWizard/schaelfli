module Geometry.Grid.View
  exposing
    ( viewAt
    )


import Svg
  exposing
    ( Svg
    )
import Svg.Attributes
  as Attributes


import Geometry.Grid
  as Grid
  exposing
    ( Grid
    )
import Geometry.Position
  as Position
  exposing
    ( Position
    )


cellAt : Int -> Int -> Maybe Char -> Svg msg
cellAt x y maybeChar =
  let
    square : String -> Svg msg
    square color =
      Svg.rect
        [ x + 2 |> String.fromInt |> Attributes.x
        , y + 2 |> String.fromInt |> Attributes.y
        , Attributes.height "96"
        , Attributes.width "96"
        , Attributes.rx "5"
        , Attributes.ry "5"
        , Attributes.fill "none"
        , Attributes.stroke color
        , Attributes.strokeWidth "4"
        ]
        [
        ]
  in
    case
      maybeChar
    of
      Nothing ->
        square "lightgrey"
      Just char ->
        Svg.svg
          [
          ]
          [ Svg.text_
            [ x + 50 |> String.fromInt |> Attributes.x
            , y + 50 |> String.fromInt |> Attributes.y
            ]
            [ Svg.text (String.fromChar char)
            ]
          , square "black"
          ]


viewAt : Position -> Grid -> Svg msg
viewAt pos grid =
  Svg.svg
    [ Attributes.width "320"
    , Attributes.height "320"
    , Attributes.viewBox "0 0 320 320"
    ]
    [ pos
      |> Grid.getCell grid |> cellAt 110 110
    , pos |> Position.step
      |> Grid.getCell grid |> cellAt 110 10
    , pos |> Position.rotateLeft |> Position.step
      |> Grid.getCell grid |> cellAt 10 110
    , pos |> Position.rotateLeft |> Position.rotateLeft |> Position.step
      |> Grid.getCell grid |> cellAt 110 210
    , pos |> Position.rotateRight |> Position.step
      |> Grid.getCell grid |> cellAt 210 110
    ]
