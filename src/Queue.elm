module Queue
  exposing
    ( Queue
    , singleton
    , push
    , pushes
    , pop
    )


type alias Queue a =
  { enqueue :
    List a
  , dequeue :
    List a
  }


singleton : a -> Queue a
singleton x =
  { enqueue =
    [ x ]
  , dequeue =
    [ ]
  }


push : a -> Queue a -> Queue a
push x { enqueue, dequeue } =
  Queue enqueue (x :: dequeue)


pushes : List a -> Queue a -> Queue a
pushes xs { enqueue, dequeue } =
  Queue enqueue (xs ++ dequeue)


pop : Queue a -> Maybe (a, Queue a)
pop queue =
  case
    queue.enqueue
  of
    x :: xs ->
      Just (x, Queue xs queue.dequeue)
    [] ->
      case
        List.reverse queue.dequeue
      of
        x :: xs ->
          Just (x, Queue xs [])
        [] ->
          Nothing
