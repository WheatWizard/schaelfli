module View
  exposing
    ( view
    )


import Browser
import Dict
import Html
import Html.Attributes
  as Attributes
import Html.Events
  as Events


import Geometry.Grid.View
  as Grid
import Geometry.Position
  as Position
import Type.Model
  exposing
    ( Model (..)
    )
import Type.Message
  as Message
  exposing
    ( Message
    )


view : Model -> Browser.Document Message
view (Model { source, program, position }) =
  { title =
    "Hello world"
  , body =
    [ Html.div
      [
      ]
      [ Html.textarea
        [ Attributes.placeholder "Source"
        , Attributes.value source
        , Events.onInput Message.Change
        ]
        [
        ]
      , Html.div [] []
      , Html.button
        [ Events.onClick Message.Compile
        ]
        [ Html.text "Compile"
        ]
      , Html.div [] []
      , Grid.viewAt position program
      , Html.div [] []
      , Html.button
        [ Events.onClick Message.TurnLeft
        ]
        [ Html.text "Left"
        ]
      , Html.button
        [ Events.onClick Message.Step
        ]
        [ Html.text "Step"
        ]
      ]
    ]
  }
